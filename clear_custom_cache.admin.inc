<?php

/**
 * @file
 * Provides functions for Administration settings.
 */

/**
 * Cache Clear table form.
 *
 * @ingroup forms
 */
function clear_custom_cache_form($form, &$form_state) {
  $cache_table_list = array();
  $cache_name = db_query("SHOW TABLES LIKE '%cache%'")->fetchField();
  $table_name = explode('_', $cache_name);
  // Table Start with Prefix.
  $result = db_query("SHOW TABLES LIKE '%cache%'")->fetchAll();
  if ($table_name[1] == 'cache') {
    foreach ($result as $value) {
      foreach ($value as $val) {
        $cache_table_list[(str_replace($table_name[0] . '_', "", $val))] = (str_replace($table_name[0] . '_', "", $val));
      }
    }
  }
  // Table Start without Prefix.
  else {
    foreach ($result as $value) {
      foreach ($value as $val) {
        $cache_table_list[$val] = $val;
      }
    }
  }
  $bin_option = array('NULL' => 'Select bin') + $cache_table_list;
  $wild_option = array(0 => 'No', 1 => 'Yes');
  $form['cid'] = array(
    '#type' => 'textfield',
    '#title' => 'Cache Id',
    '#attributes' => array('placeholder' => t('cache_id')),
    '#description' => t("Example : cache_set('cache_id', 'my_data', 'cache'); Here cid => cache_id"),
  );
  $form['cache_bin'] = array(
    '#type' => 'select',
    '#title' => 'Cache Bin',
    '#options' => $bin_option,
    '#default_value' => $bin_option['cache'],
    '#description' => t("Example : cache_set('cache_id', 'my_data', 'cache'); Here bin => cache"),
  );

  $form['wild_card'] = array(
    '#type' => 'select',
    '#title' => 'Wildcards',
    '#options' => $wild_option,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear Cache'),
  );
  return $form;
}

/**
 * Form validation handler for clear_custom_cache_form().
 *
 * @ingroup forms
 */
function clear_custom_cache_form_validate($form, &$form_state) {
  if (empty($form_state['values']['cid']) && $form_state['values']['cache_bin'] == 'NULL') {
    form_set_error('cid', t('Select Atleat One (cid Or cache_bin)!!'));
  }
}

/**
 * Form submission handler for clear_custom_cache_form().
 *
 * @ingroup forms
 */
function clear_custom_cache_form_submit($form, &$form_state) {
  $cache_cid = $form_state['values']['cid'];
  $cache_bin = $form_state['values']['cache_bin'];
  $cache_wcard = $form_state['values']['wild_card'];
  cache_clear_all($cache_cid, $cache_bin, $cache_wcard);
  drupal_set_message(t('Cache has been cleared successfully .'));
}
