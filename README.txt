
INTRODUCTION
------------

The Clear Custom Cache module provides a feature 
to Clear chache via cid


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.
Extract the 'clear_custom_cache' folder and put in to modules directory

CONFIGURATION
-------------

  * Configure settings at Admin » config » development » Clear Custom Cache
