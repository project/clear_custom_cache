
INTRODUCTION
------------

The Clear Custom Cache module provides a feature 
to Clear chache via cid


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.
Extract the 'clear_custom_cache' folder and put in to modules directory

CONFIGURATION
-------------

  * Configure settings at Admin » config » development » Clear Custom Cache


CONTENTS OF THIS FILE
---------------------
 * Description
 * Introduction
 * Requirements
 * Installing
 * Uninstalling
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * More Information

DESCRIPTION
-----------
The Clear Custom Cache module provides a feature 
stored in cache table in database via cid.

INTRODUCTION
------------

Current Maintainer: Dhruv Panchal <https://www.drupal.org/u/dhruv-panchal>

Clear custom cache use to clear db cache
table

REQUIREMENTS
------------

"No special requirements"

INSTALLATION
----------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once Youtube channel subscribe  module is installed and enabled,
you can adjust the settings for your
site's at admin/config/media/youtube_subscriber

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
When enabled, the module will prevent the links from appearing.
To get the links back, disable the module and clear caches.

UNINSTALLING
------------

You can easily uninstall module from module list, there is no any dependency


FREQUENTLY ASKED QUESTIONS (FAQ)
--------------------------------

- There are no frequently asked questions at this time.


KNOWN ISSUES
------------

Error - Error at the place of button
Solution :  Please confirm that you placed right channel name 
or channel id in configuration page


MORE INFORMATION
----------------

- There are no more information.

