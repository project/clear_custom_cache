<?php

namespace Drupal\clear_custom_cache\Form;

/**
 * @file
 * Provides clear_custom_cache functionality.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\CacheBackendInterface;

class ClearCustomCacheForm extends FormBase
{
    
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'clear_custom_cache_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        // Fetch all table from database which have cache as a word in table name.
        $result = db_query("SHOW TABLES LIKE '%cache%'")->fetchAll();
        foreach ($result as $result_array) {
            foreach ($result_array as $records) {
				$get_cache_table_name = str_replace("cache_" , "" , $records);
                $records_array[$get_cache_table_name] = $records;
            }
        }
        // Emtpy array to push in select option field.
        $emtpy_array = array(
            'Select Bin'
        );
        $array_merge = array_merge($emtpy_array, $records_array);
        
        $form['clear_custom_cache_label_details'] = array(
            '#type' => 'item',
            '#markup' => $this->t('This module used to clear cache via cid Or bin basis like cache, cache_block, cache_menu etc. Or combination of both cid+bin.'),
        );
        
        $form['cid'] = array(
            '#type' => 'textfield',
            '#title' => t('Cache Id'),
            '#attributes' => array(
                'placeholder' => t('cache_id')
            ),
            '#description' => 'Example : cache_set(cache_id, my_data, cache); Here cid => cache_id',
        );
        
        $form['cache_bin'] = array(
            '#type' => 'select',
            '#title' => t('Cache Bin'),
            '#options' => $array_merge,
        );

        $form['submit']         = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
		if(empty($form_state->getValue('cid')) && $form_state->getValue('cid') == 'NULL') {
            $form_state->setErrorByName('cid', $this->t('Select Atleat One (cid Or cache_bin)!!'));
        }
    }    
    
    public function submitForm(array &$form, FormStateInterface $form_state) {
          $cache_cid = $form_state->getValue('cid');
		  $cache_bin = $form_state->getValue('cache_bin');
		  \Drupal::cache($cache_bin)->delete($cache_cid);
		  drupal_set_message(t('Cache has been cleared successfully .'));
    }
    
}
